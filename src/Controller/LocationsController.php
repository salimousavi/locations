<?php

namespace Drupal\locations\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Created by PhpStorm.
 * User: kasper
 * Date: 1/31/17
 * Time: 11:11 AM
 */
class LocationsController {

    public function index() {

        $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['vid' => 'locations']);

        $weathers = false;
        if (\Drupal::service('module_handler')->moduleExists('weathers')) {
            $weathers = true;
        }

        usort($terms, function ($a, $b) {
            if ($a->getWeight() == $b->getWeight()) {
                return 0;
            }

            return ($a->getWeight() < $b->getWeight()) ? -1 : 1;
        });

        $result = [];
        foreach ($terms as $term) {

            $machine_name = $term->get('field_machine_name')->getValue()[0]['value'];
            $r = array(
                'name'         => $term->getName(),
                'machine_name' => $machine_name,
                'lat'          => $term->get('field_latitude')->getValue()[0]['value'],
                'long'         => $term->get('field_longitude')->getValue()[0]['value'],
                'zone'         => $term->get('field_time_zone')->getValue()[0]['value'],
                'language'     => $term->get('field_language')->getValue()[0]['value'],
                'diff'         => get_timezone_offset('UTC', $term->get('field_time_zone')->getValue()[0]['value']),
            );

            if ($weathers) {
                $r['summary'] = $term->get('field_weather_summary')->getValue()[0]['value'];
                $r['temperature'] = $term->get('field_weather_temperature')->getValue()[0]['value'];
                $r['status'] = $term->get('field_weather_status')->getValue()[0]['value'];
                $r['humidity'] = $term->get('field_weather_humidity')->getValue()[0]['value'];
                $r['wind_speed'] = $term->get('field_weather_wind_speed')->getValue()[0]['value'];
                $r['date'] = $term->get('field_weather_date')->getValue()[0]['value'];
            }

            $result[] = $r;
        }

        return new JsonResponse($result);
    }

}