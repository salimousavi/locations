# Locations

<h3> Installation </h3>

Download this module and add in module directory and enable it.

<h3> How to work? </h3>
This module create a vocabulary that named <code>locations</code>.<br>

<b>Fields:</b>
 <br><code> field_latitude </code>
 <br><code> field_longitude </code>
 <br><code> field_time_difference </code>
 <br><code> field_machine_name </code>

<b>Route request:</b>
 <br>
 <code> api/locations </code>

<b>Response:</b>
 <br><code> name </code> Example: Tehran
 <br><code> machine_name </code> Example: tehran
 <br><code> lat </code> Example: 35.7299926
 <br><code> long </code> Example: 51.4166642
 <br><code> diff </code> Example: +3.5